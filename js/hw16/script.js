function findFib(n) {
  n = +prompt('Введите порядковый номер числа Фибоначчи');

  function fib(n) {
    if (n === 1 || n === 0) {
      return n;
    } else if (n > 0) {
      return fib(n - 1) + fib(n - 2);
    } else {
      return fib(n + 2) - fib(n + 1);
    }
  }

  return fib(n);
}

console.log(findFib());