function createNewUser(firstName, lastName, birthday) {
  firstName = prompt('Назовите ваше имя.');
  lastName = prompt('Назовите вашу фамилию');
  birthday = prompt('Назовите вашу дату рождения', 'dd.mm.yyyy');

  const newUser = {
    firstName: firstName,
    lastName: lastName,
    birthday: birthday,

    getLogin() {
      const login = this.firstName[0] + this.lastName;
      return login.toLowerCase();
    },

    setFirstName(newFirstName) {
      Object.defineProperty(newUser, 'firstName', {
        writable: false,
        value: newFirstName
      });
      return this.firstName;
    },

    setLastName(newLastName) {
      Object.defineProperty(newUser, 'lastName', {
        writable: false,
        value: newLastName
      });
      return this.lastName;
    },

    getAge() {
      if (this.birthday !== null) {
        const YEAR = 31536000000;
        const now = new Date();

        const splittedDate = this.birthday.split('.');
        splittedDate.reverse();
        const joinedDate = splittedDate.join('-');

        const userBirthday = new Date(joinedDate);
        const age = Math.floor((now.getTime() - userBirthday.getTime()) / YEAR);
        return age;
      }
    },

    getPassword() {
      const password = (this.firstName[0]).toUpperCase() + (this.lastName).toLowerCase() + this.getAge();
      return password;
    }
  };

  Object.defineProperty(newUser, 'firstName', {
    writable: false,
  });
  Object.defineProperty(newUser, 'lastName', {
    writable: false,
  });

  return newUser;
}

const newUser = createNewUser();

console.log(newUser);
console.log(newUser.getAge());
console.log(newUser.getPassword());