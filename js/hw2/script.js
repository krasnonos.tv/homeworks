let userNumber;
let multiple;

while (!Number.isInteger(userNumber)) {
  userNumber = +prompt('Enter an integer');
}

if (userNumber > 5) {
  for (let multiple = 0; multiple <= userNumber; multiple++) {
    if (multiple % 5 === 0) {
      console.log(multiple);
    }
  }
} else {
  console.log('Sorry, no numbers');
}


let m = +prompt('Enter a number');
let n = +prompt('Enter a number that is greater than previous one');

if (m > n) {
  alert('Error!');
  m = +prompt('Enter a number');
  n = +prompt('Enter a number that is greater than previous one');
}

outer:
  for (let i = m; i <= n; i++) {

    for (let j = 2; j < i; j++) {
      if (i % j === 0) continue outer;
    }

    console.log(i);
  }