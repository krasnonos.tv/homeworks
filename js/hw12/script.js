'use strict';

//создаем кнопку "Прекратить"
const btnStop = document.createElement('button');
const imagesWrapper = document.querySelector('.images-wrapper');
imagesWrapper.after(btnStop);
btnStop.className = 'btn stop';
btnStop.textContent = 'Прекратить';

//создаем кнопку "Возобновить показ"
const btnResume = document.createElement('button');
btnStop.after(btnResume);
btnResume.className = 'btn resume';
btnResume.textContent = 'Возобновить показ';


const images = document.querySelectorAll('img');
const imagesSrc = [];

for (const img of images) {
  imagesSrc.push(img.getAttribute('src'));
}

const length = imagesSrc.length;
let index = 1;
let timer;

function showPictures() {
  timer = setInterval(() => {
    btnResume.setAttribute('disabled', 'true');
    if (index === length) {
      index = 0;
    }
    document.querySelector('.image-to-show').src = imagesSrc[index++];
  }, 1000);
}

function stopShowingPictures() {
  clearInterval(timer);
  btnResume.removeAttribute('disabled');
}

showPictures();

btnStop.addEventListener('click', stopShowingPictures);
btnResume.addEventListener('click', showPictures);