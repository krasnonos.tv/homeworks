const body = document.querySelector('body');
const btn = document.querySelector('.btn');
const img = document.querySelector('img');

if (localStorage.getItem('theme') === 'light-theme') {
  body.classList.remove('dark-theme');
  body.classList.add('light-theme');
} else {
  body.classList.remove('light-theme');
  body.classList.add('dark-theme');
  img.src = 'images/image-dark.png';
}

btn.addEventListener('click', () => {
  body.classList.toggle('light-theme');
  body.classList.toggle('dark-theme');

  if (body.classList.contains('dark-theme')) {
    img.src = 'images/image-dark.png';
  }
  if (body.classList.contains('light-theme')) {
    img.src = 'images/image.png';
  }

  localStorage.setItem('theme', body.className);
});