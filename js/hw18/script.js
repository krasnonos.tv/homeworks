function clone(obj) {
  const cloneObj = {};

  for (let key in obj) {
    if (obj[key] instanceof Date) {
      cloneObj[key] = new Date(obj[key]);
    } else if (Array.isArray(obj[key])) {
      cloneObj[key] = [];
      for (let i of obj[key]) {
        cloneObj[key].push(i);
      }
    } else if (typeof obj[key] === 'object' && obj[key] !== null) {
      cloneObj[key] = clone(obj[key]);
    } else {
      cloneObj[key] = obj[key];
    }
  }
  return cloneObj;
}

const test = {
  a: 1,
  b: '2',
  c: true,
  d: null,
  e: {
    aa: 3,
    ad: ['bla', 'bla', 'bla'],
    ae: new Date('2020-03-17')
  }
};

const clonedTest = clone(test);

test.e.aa = 144;
test.e.ad.push('changes');
test.e.ae = new Date('1990-02-07');

console.log(test);
console.log(clonedTest);
console.log(clonedTest.e.ad);