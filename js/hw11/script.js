function highlightKey() {
  document.addEventListener('keydown', (event) => {
    const btns = document.querySelectorAll('.btn');
    const letter = event.key;

    btns.forEach((item) => {
      if (item.style.backgroundColor === 'blue') {
        item.style.backgroundColor = 'black';
      }
      if (letter === item.textContent || letter.toUpperCase() === item.textContent) {
        item.style.backgroundColor = 'blue';
      }
    })
  })
}

highlightKey();