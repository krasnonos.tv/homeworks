function findFactorial(n) {
  n = prompt('Введите число.');

  if (isNaN(n) || n === '') {
    n = prompt('Введите число.', `${n}`);
  }

  function factorial(n) {
    return (n === 1) ? 1 : n * factorial(n - 1);
  }

  if (n !== null && !isNaN(n)) {
    return factorial(n);
  }
}

console.log(findFactorial());