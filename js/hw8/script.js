const form = document.createElement('form');
document.body.append(form);

const label = document.createElement('label');
form.append(label);
label.textContent = 'Price';
label.style.cssText = `
  padding: 5px;
  font-size: 20px;
`;

const input = document.createElement('input');
label.append(input);
input.setAttribute('type', 'number');
input.style.cssText = `
  margin: 15px;
  padding: 5px;
  border: 3px solid black;
  border-radius: 5px;
  outline: none;
`;

const span = document.createElement('span');

const btn = document.createElement('button');
btn.innerHTML = '&times;';
btn.style.cssText = `
margin-left: 15px;
background: transparent;
border-radius: 50%;
cursor: pointer;
outline: none;
`;

input.addEventListener('focus', () => {
  input.style.borderColor = 'green';
  input.style.color = 'black';
});

input.addEventListener('blur', () => {
  if (input.value < 0 && input.value !== '') {
    btn.remove();
    form.after(span);
    span.textContent = 'Please enter correct price';
    input.style.borderColor = 'red';
  } else if (input.value >= 0 && input.value !== '') {
    form.before(span);
    span.after(btn);
    span.textContent = `Текущая цена: ${input.value}`;
    input.style.borderColor = 'black';
    input.style.color = 'green';

    btn.addEventListener('click', () => {
      span.remove();
      btn.remove();
      input.value = '';
    });
  } else {
    span.remove();
    btn.remove();
  }
});