const name = prompt('Назовите ваше имя');
const lastName = prompt('Назовите вашу фамилию');

const student = {
  name: name,
  lastName: lastName
};

student.tabel = {};
let subject = prompt('Назовите предмет');

while (subject) {
  student.tabel[subject] = +prompt('Какая оценка по предмету?');
  subject = prompt('Назовите предмет');
}

let badGrades = 0;
let gradesSum = 0;
let gradesQuantity = 0;

for (let key in student.tabel) {
  if (student.tabel[key] < 4) {
    badGrades++
  }
  gradesSum += student.tabel[key];
  gradesQuantity++
}

if (badGrades === 0) {
  console.log('Студент переведен на следующий курс');
}

const gradePointAverage = gradesSum / gradesQuantity;

if (gradePointAverage > 7) {
  console.log('Студенту назначена стипендия');
}