function checkDeadline(velocity, backlog, deadline) {
  const DAY = 86400000;
  const workingDay = 8;

  let teamVelocityPerDay = 0;
  let backlogSum = 0;

  for (let i = 0; i < velocity.length; i++) {
    teamVelocityPerDay += velocity[i];
  }

  for (let i = 0; i < backlog.length; i++) {
    backlogSum += backlog[i];
  }

  const daysForWork = backlogSum / teamVelocityPerDay;

  const deadlineTimestamp = deadline.getTime();

  const now = new Date();
  const nowTimestamp = now.getTime();

  const daysBeforeDeadline = (deadlineTimestamp - nowTimestamp) / DAY;

  if ((daysForWork) > (daysBeforeDeadline)) {
    alert(`Команде разработчиков придется потратить дополнительно ${Math.ceil((daysForWork - daysBeforeDeadline) * workingDay)} часов после дедлайна, чтобы выполнить все задачи в беклоге`);
  } else {
    alert(`Все задачи будут успешно выполнены за ${Math.ceil(daysBeforeDeadline - daysForWork)} дней до наступления дедлайна!`);
  }
}

const velocity1 = [5, 7, 9, 15];
const backlog1 = [20, 30, 40];
const deadline1 = new Date(2020, 4, 31);

console.log(checkDeadline(velocity1, backlog1, deadline1));

const velocity2 = [5, 7, 9, 15, 10];
const backlog2 = [20, 30, 40, 15, 20, 140];
const deadline2 = new Date(2020, 4, 29);

console.log(checkDeadline(velocity2, backlog2, deadline2));