'use strict';

const btn = document.querySelector('.btn');
const wrapper = document.querySelector('.wrapper');
const ul = document.createElement('ul');
wrapper.append(ul);

btn.addEventListener('click', (event) => {
  event.preventDefault();

  fetch('https://api.ipify.org/?format=json')
    .then(response => {
      return response.json();
    })
    .then((result) => {
      const ip = result.ip;
      return ip;
    })
    .then((ip) => {
      fetch(`http://ip-api.com/json/${ip}`)
        .then((response) => {
          return response.json();
        })
        .then((result) => {
          ul.innerHTML = `<li> Continent: ${result.timezone}</li>
              <li> Country: ${result.country}</li>
              <li> Region: ${result.region}</li>
              <li> City: ${result.city}</li>
              <li> District: ${result.zip}</li>`;
          wrapper.append(ul);
        })
    });
});