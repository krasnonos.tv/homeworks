'use strict';

const books = [{
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70
  },
  {
    author: "Скотт Бэккер",
    name: "Воин-пророк",
  },
  {
    name: "Тысячекратная мысль",
    price: 70
  },
  {
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70
  },
  {
    author: "Дарья Донцова",
    name: "Детектив на диете",
    price: 40
  },
  {
    author: "Дарья Донцова",
    name: "Дед Снегур и Морозочка",
  }
];

const ul = document.createElement('ul');
document.body.querySelector('#root').append(ul);

function validateObject(object) {
  for (let item of object) {
    try {
      if (!item.name) {
        throw new Error('Данные неполны: нет названия');
        continue;
      } else if (!item.author) {
        throw new Error('Данные неполны: нет автора');
        continue;
      } else if (!item.price) {
        throw new Error('Данные неполны: нет цены');
        continue;
      }
      const li = document.createElement('li');
      ul.append(li);
      li.innerText = `${item.name}, автор ${item.author}, цена - ${item.price};`;
    } catch (error) {
      console.log(error.message);
    }
  }
}

validateObject(books);