function createList(array) {
  const ul = document.createElement('ul');
  document.body.append(ul);
  const listItems = array.map(text => `<li>${text}</li>`);
  return ul.innerHTML = listItems.join('');
}

const test = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];
createList(test);