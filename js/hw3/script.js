let number1 = prompt('Введите первое число');

if (isNaN(number1) || number1 === '') {
  number1 = prompt('Введите первое число', `${number1}`);
}

let number2 = prompt('Введите второе число');

if (isNaN(number2) || number1 === '') {
  number2 = prompt('Введите второе число', `${number2}`);
}

number1 = +number1;
number2 = +number2;

let operation = prompt('Какую математичскую операцию нужно совершить?');

function count(number1, number2, operation) {
  switch (operation) {
    case '+':
      return number1 + number2;
    case '-':
      return number1 - number2;
    case '*':
      return number1 * number2;
    case '/':
      if (number2 !== 0) {
        return number1 / number2;
      } else {
        return 'На 0 делить нельзя';
      }
  }
}

console.log(count(number1, number2, operation));