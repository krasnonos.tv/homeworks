'use strict';

const btn = document.querySelector('.btn');
const wrapper = document.querySelector('.wrapper');
const ul = document.createElement('ul');
wrapper.append(ul);

btn.addEventListener('click', async (event) => {
  event.preventDefault();

  const response = await fetch('https://api.ipify.org/?format=json');
  const ip = await response.json();

  const response2 = await fetch(`http://ip-api.com/json/${ip.ip}`);
  const result = await response2.json();

  ul.innerHTML = `<li> Continent: ${result.timezone}</li>
            <li> Country: ${result.country}</li>
            <li> Region: ${result.region}</li>
            <li> City: ${result.city}</li>
            <li> District: ${result.zip}</li>`;
  wrapper.append(ul);
});