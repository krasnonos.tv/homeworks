function filterBy(array, dataType) {
  return array.filter(item => {
    if (dataType === 'null') {
      return item !== null;
    } else {
      return typeof item !== dataType;
    }
  });
}

const test = ['hello', 'world', 23, '23', null];
console.log(filterBy(test, 'string'));