**Цикл forEach**

Цикл forEach - это метод массива, который в качестве аргумента принимает callback функцию. Этот цикл перебирает каждый элемент массива и выполняет над ним действие, переданное в callback функции.