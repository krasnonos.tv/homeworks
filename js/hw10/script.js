'use strict';

const crossedEyeClass = 'fa-eye-slash';
const eyeClass = 'fa-eye';
const form = document.querySelector('form');
const icons = document.querySelectorAll('i');
const firstInput = document.querySelector('.first');
const secondInput = document.querySelector('.second');
const warning = document.createElement('p');

warning.textContent = 'Нужно ввести одинаковые значения';
warning.style.color = 'red';
warning.style.marginTop = 0;

icons.forEach((icon) => icon.addEventListener('click', changeMode));
form.addEventListener('submit', comparePasswords);

function changeMode(event) {
  if (event.target.classList.contains(eyeClass)) {
    enableTextMode(event);
  } else {
    enablePasswordMode(event);
  }
}

function enableTextMode(event) {
  event.target.classList.remove(eyeClass);
  event.target.classList.add(crossedEyeClass);
  event.target.closest('label').querySelector('input').type = 'text';
}

function enablePasswordMode(event) {
  event.target.classList.remove(crossedEyeClass);
  event.target.classList.add(eyeClass);
  event.target.closest('label').querySelector('input').type = 'password';
}

function comparePasswords(event) {
  if (firstInput.value === secondInput.value) {
    warning.remove();
    setTimeout(() => alert('You are welcome!'));
  } else {
    secondInput.after(warning);
  }
  event.preventDefault();
}