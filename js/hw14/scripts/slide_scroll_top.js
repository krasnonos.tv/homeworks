const btn = $('<button>', {
  class: 'scroll-btn',
  text: 'Up',
  click: getScroll,
  css: {
    display: 'none',
    padding: '10px',
    position: 'fixed',
    bottom: '25px',
    right: '25px',
    backgroundColor: '#161619',
    color: '#18B4CF',
    fontSize: '18px',
    fontWeight: 600,
    borderRadius: '7px',
    cursor: 'pointer',
    outline: 'none'
  }
}).appendTo('body');

$(window).on("scroll", function () {
  const windowHeight = $(window).height();
  const windowScroll = $(window).scrollTop();

  if (windowHeight < windowScroll) {
    btn.fadeIn(200);
  } else {
    btn.fadeOut(200);
  }
});

function getScroll() {
  $(document.scrollingElement).animate({
    scrollTop: 0
  }, 700).scrollTop = 0;
}