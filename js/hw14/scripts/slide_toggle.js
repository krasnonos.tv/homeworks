const btnToggle = $('.btn-toggle');

btnToggle.click(function (event) {
  if (btnToggle.text() === 'Hide') {
    btnToggle.text('Show');
  } else {
    btnToggle.text('Hide');
  }
  $('#gallery').slideToggle(700);
});