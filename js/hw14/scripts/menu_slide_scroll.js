$('a[href^="#"]').click(function (event) {
  const target = $(this).attr('href');

  $('html, body').animate({
    scrollTop: $(target).offset().top
  }, 700);
});