class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }

  get name() {
    return this._name;
  }
  set name(name) {
    this._name = name;
  }
  get age() {
    return this._age;
  }
  set age(age) {
    this._age = age;
  }
  get salary() {
    return this._salary;
  }
  set salary(salary) {
    this._salary = salary;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }
  get salary() {
    return super.salary * 3;
  }
  set salary(salary) {
    super.salary = salary;
  }
}

const firstProgrammer = new Programmer('John', 'Doe', 1000, 'english');
const secondProgrammer = new Programmer('Jane', 'Boyle', 2000, 'german');
const thirdProgrammer = new Programmer('Jake', 'Smith', 3000, 'french');

console.log(firstProgrammer);
console.log(firstProgrammer.salary);
console.log(secondProgrammer);
console.log(secondProgrammer.salary);
console.log(thirdProgrammer);
console.log(thirdProgrammer.salary);