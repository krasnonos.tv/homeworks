function createNewUser(firstName, lastName) {
  firstName = prompt('Назовите ваше имя.');
  lastName = prompt('Назовите вашу фамилию');
  const newUser = {
    firstName: firstName,
    lastName: lastName,

    getLogin() {
      const login = this.firstName[0] + this.lastName;
      return login.toLowerCase();
    },

    setFirstName(newFirstName) {
      Object.defineProperty(newUser, 'firstName', {
        writable: false,
        value: newFirstName
      });
      return this.firstName;
    },

    setLastName(newLastName) {
      Object.defineProperty(newUser, 'lastName', {
        writable: false,
        value: newLastName
      });
      return this.lastName;
    }
  };

  Object.defineProperty(newUser, 'firstName', {
    writable: false,
  });
  Object.defineProperty(newUser, 'lastName', {
    writable: false,
  });

  return newUser;
}

const newUser = createNewUser();

console.log(newUser.getLogin());

newUser.firstName = 'Sasha';
console.log({
  ...newUser
});

newUser.setLastName('Kravchenko');
console.log(newUser);