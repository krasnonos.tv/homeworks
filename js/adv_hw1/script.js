'use strict';

//Задание №1
const cities = ["Kyiv", "Berlin", "Dubai", "Moscow", "Paris"];
const [city1, city2, city3, city4, city5] = cities;
console.log(city1, city2, city3, city4, city5);


//Задание №2
const employee = {
  name: 'John',
  salary: 500
};

const {
  name,
  salary
} = employee;

console.log(name, salary);


//Задание №3
const array = ['value', 'showValue'];
const [value, showValue] = array;

alert(value);
alert(showValue);