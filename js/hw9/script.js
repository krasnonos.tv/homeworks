const tabs = document.querySelector('.tabs');
const tabsTitles = document.querySelectorAll('.tabs-title');
const paragraphs = document.querySelectorAll('.tabs-content li');
const className = 'active';

const tabsTitlesArr = Array.from(document.querySelectorAll('.tabs-title'));
const paragraphsArr = Array.from(document.querySelectorAll('.tabs-content li'));

showCurrentTab();
tabs.addEventListener('click', changeClass);

function changeClass(event) {
  const tab = event.target.closest('li');

  for (const elem of tabsTitles) {
    elem.classList.remove(className);
  }

  tab.classList.add(className);
  showCurrentTab();
}

function showCurrentTab() {
  for (const tab of tabsTitles) {
    if (tab.classList.contains('active')) {
      for (const el of paragraphs) {
        el.style.display = 'none';
      }
      const index = tabsTitlesArr.findIndex(element => element.classList.contains('active'));
      paragraphsArr[index].style.display = 'block';
    }
  }
}