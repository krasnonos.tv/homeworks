import styled from 'styled-components';

export const ModalOverlay = styled.div`
    background-color: rgba(0, 0, 0, 0.5);
    position: fixed;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
`;

export const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100vh;
`;

export const Btn = styled.button`
    margin: 20px;
    padding: 9px 15px;
    min-width: 102px;
    min-height: 42px;
    border: none;
    border-radius: 4px;
    outline: none;
    color: #fff;
    cursor: pointer;
    
        &:active {
        box-shadow: 1px 1px 4px black;;
        }
    `;

export const ModalWindow = styled.div`
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    max-width: 40%;
    background-color: ${props => props.theme.mainColor};
    border-radius: 4px;
    color: #fff;
    `;

export const ModalHeader = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 0 20px;
    border-radius: 4px;
    background-color: ${props => props.theme.headerColor};
`;

export const ModalBody = styled.div`
    padding: 10px;
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    align-items: center;
    text-align: center;
    line-height: 30px;
`;

export const Title = styled.h1`
    font-size: 15px;
    margin: 25px;
`;

export const TextModal = styled.p`
    font-size: 15px;
    line-height: 25px;
    margin: 30px;
    width: 100%;
    text-align: center;
`;

export const Cross = styled.span`
    font-size: 20px;
    cursor: pointer;
`;
