import React from 'react';
import PropTypes from 'prop-types';

import { ModalOverlay, ModalWindow, ModalHeader, Title, ModalBody, TextModal, Cross } from '../styles/index';

export const Modal = ({ theme, header, closeButton, text, actions, hideModal }) => {
  // const [isActive, setIsActive] = useState(true);

  // const hideModal = () => {
  //   window.addEventListener('click', (e) => {
  //     setIsActive(!isActive);
  //   }
  //   );
  // };

  return (
    <>
      <ModalOverlay className="modal" onClick={hideModal}>
        <ModalWindow theme={theme}>
          <ModalHeader theme={theme}>
            <Title>{header}</Title>
            {closeButton && <Cross className="cross" onClick={hideModal}>X</Cross>}
          </ModalHeader>
          <ModalBody>
            <TextModal>
              {text}
            </TextModal>
            {actions[0]}
            {actions[1]}
          </ModalBody>
        </ModalWindow>
      </ModalOverlay>
    </>
  );
};

Modal.propTypes = {
  header: PropTypes.string,
  closeButton: PropTypes.bool,
  text: PropTypes.string,
  actions: PropTypes.array
};

Modal.defaultProps = {
  header: 'Default title',
  closeButton: true,
  text: 'Default text',
};