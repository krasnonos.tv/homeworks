import React from 'react';
import PropTypes from 'prop-types';

import { Btn } from '../styles/index';

export const Button = ({ text, backgroundColor, onClick }) => {
  return (
    <Btn style={{ background: backgroundColor }} onClick={onClick}>{text}</Btn>
  );
};

Button.propTypes = {
  text: PropTypes.string,
  backgroundColor: PropTypes.string,
  onClick: PropTypes.func
};

Button.defaultProps = {
  text: 'Default button',
  backgroundColor: 'fff',
  onClick: () => { }
};