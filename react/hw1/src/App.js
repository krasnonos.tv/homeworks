import React, { useState } from 'react';

import { Button } from './components/Button';
import { Modal } from './components/Modal';
import { Container } from './styles/index';


function App() {
  const [openFirstModal, setOpenFirstModal] = useState(false);
  const [openSecondModal, setOpenSecondModal] = useState(false);

  const showFirstModal = () => setOpenFirstModal(!openFirstModal);
  const showSecondModal = () => setOpenSecondModal(!openSecondModal);

  const hideFirstModal = (e) => {
    if (e.target === document.querySelector('.modal') || e.target === document.querySelector('.cross')) {
      setOpenFirstModal(!openFirstModal);
    }
  };

  const hideSecondModal = (e) => {
    if (e.target === document.querySelector('.modal') || e.target === document.querySelector('.cross')) {
      setOpenSecondModal(!openSecondModal);
    }
  };

  return (
    <div className="App">
      <Container>

        <Button text="Open first Modal" backgroundColor="grey" onClick={showFirstModal} />

        {openFirstModal && <Modal
          theme={{ mainColor: "#E74C3C", headerColor: "#D44637" }}
          header="Do you want to delete this file?"
          text="Once you delete this file, it won't be possible to undo this action. Are you sure you want to delete it?"
          closeButton={true}
          hideModal={hideFirstModal}
          actions={[
            <Button text="Ok" backgroundColor="#B3382C" onClick={() => console.log("Ok")} />,
            <Button text="Cancel" backgroundColor="#B3382C" onClick={() => console.log("Cancel")} />
          ]}
        />}

        <Button text="Open second Modal" backgroundColor="black" onClick={showSecondModal} />

        {openSecondModal && <Modal
          theme={{ mainColor: "#6c96f9", headerColor: "#3d78ff" }}
          header="Wait!"
          text="We want to give a 10% discount for your first order."
          closeButton={true}
          hideModal={hideSecondModal}
          actions={[
            <Button text="Ok" backgroundColor="#8869ff" onClick={() => console.log("Ok")} />,
            <Button text="Cancel" backgroundColor="#f9a4ff" onClick={() => console.log("Cancel")} />
          ]}
        />}

      </Container>
    </div>
  );
}

export default App;
