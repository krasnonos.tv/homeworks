document.addEventListener('DOMContentLoaded', (event) => {
  document.querySelector('.navbar__mobile-menu').addEventListener('click', (event) => {
    document.querySelector('.navbar__burger').classList.toggle('navbar__burger--active');
    document.querySelector('.navbar__list').classList.toggle('navbar__list--active');
  });
});